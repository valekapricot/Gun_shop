# Оружейны магазин "Амуниция №9"
Оружейный магазин – это мобильное приложение, предназначенное для покупки и просмотра информации о различных видов оружия. Пользователи смогут легко найти и купить оружие, а также получить полную информацию о каждом продукте.

Техническое задание
https://gitlab.com/valekapricot/Gun_shop/-/blob/main/%D0%A1%D1%8B%D1%87%D0%B5%D0%B2_%D0%92%D0%B0%D0%BB%D0%B5%D0%BD%D1%82%D0%B8%D0%BD_4%D0%98%D0%A1%D0%9F9-14_%D0%A2%D0%97_%D0%9E%D1%80%D1%83%D0%B6%D0%B5%D0%B9%D0%BD%D1%8B%D0%B9_%D0%BC%D0%B0%D0%B3%D0%B0%D0%B7%D0%B8%D0%BD.docx

Диаграмма в Visio

![image](https://github.com/ValekApricot/CH/assets/116540788/dd554789-b6e6-4944-8e0f-cb099074cf69)

Диаграмма UseCase

![image](https://github.com/ValekApricot/AdyshkinTask/assets/116540788/fb001020-8262-4662-ab8b-ded893493f9a)

Figma дизайн (не весь)

![image](https://github.com/ValekApricot/AdyshkinTask/assets/116540788/2ce13550-2123-4d24-b91a-2b1c3bd43603)

https://www.figma.com/file/doXzQUWJgikOENqvnNmY8G/Untitled?type=design&node-id=0-1&mode=design&t=eM4xHZmKjDbrR5m7-0

База данных SQL Server

![image](https://github.com/ValekApricot/AdyshkinTask/assets/116540788/111193e7-092e-4d1e-b3fb-57eb20a060cd)

![image](https://github.com/ValekApricot/AdyshkinTask/assets/116540788/be12fce1-318a-4ec5-8d66-02ed1c71f46f)

Диаграмма последовательности

![image](https://github.com/ValekApricot/AdyshkinTask/assets/116540788/4970ceda-09b6-4483-8355-34660a7fa526)

ERD Диаграмма для склада готовой продукции

![image](https://github.com/ValekApricot/AdyshkinTask/assets/116540788/32c1f495-8593-4bd5-94b2-78b1126f47e1)

Диаграмма классов

![image](https://github.com/ValekApricot/CH/assets/116540788/e30309e2-1ee6-446d-b9ca-512a08ef435e)

Диаграмма Деятельности (Активности)

![image](https://github.com/ValekApricot/CH/assets/116540788/4da37483-dbd3-41d2-b9f6-2b5115646760)

